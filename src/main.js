import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//字体请求
import './common/font/font.css'
import './assets/common.css'
import './assets/css/reset.css'; //样式重置
import { obj } from './assets/js/rem'; //自适应
import 'font-awesome/css/font-awesome.min.css'; //字体库

//axios 异步请求
import axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);

//vant
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);

// 引入swiper
import { Swipe, SwipeItem } from 'vant';

Vue.use(Swipe);
Vue.use(SwipeItem);

//map
import BaiduMap from 'vue-baidu-map'

Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: 'd44oHULzxHzBFFRqjGnIyhLb2AVe6ATO'
})

//引入筛选下拉框
import { DropdownMenu, DropdownItem } from 'vant';
Vue.use(DropdownMenu);
Vue.use(DropdownItem);

Vue.config.productionTip = false;


import Router from 'vue-router'
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}
new Vue({
  router,
  store,
  mounted() {
    // 自适应
    obj.setRem();
    window.onresize = obj.setRem;
    this.$router.push('/login')
  },
  render: h => h(App)
}).$mount('#app')
