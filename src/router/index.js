import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    redirect: '/login'
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home'),
    children: [
      //国内租车
      {
        path: '/',
        name: 'Domestic',
        redirect: 'domestic'
      },
      {
        path: 'domestic',
        name: 'Domestic',
        component: () => import('../components/Home/domestic')
      },
      // 境外租车
      {
        path: 'foreign',
        name: 'Foreign',
        component: () => import('../components/Home/foreign')
      }
    ]
  },
  //客服中心
  {
    path: '/kefuzhongxin',
    name: 'kefuzhongxin',
    component: () => import('../components/jingtai_/kefuzhongxin.vue')
  },
  // 设置
  {
    path: '/shezhi',
    name: 'shezhi',
    component: () => import('../components/jingtai_/shezhi.vue')
  },
  // 服务规则
  {
    path: '/fuwuguize/:submodule',
    name: 'fuwuguize',
    component: () => import('../components/jingtai_/fuwuguize.vue')
  },
  //客服问答
  {
    path: '/issue/:data',
    component: () => import('../components/jingtai_/issue.vue')
  },
  // 我的订单
  {
    path: '/wodedingdan',
    name: 'wodedingdan',
    component: () => import('../components/jingtai_/wodedingdan.vue')
  },
  // 评价
  {
    path: '/pingjia',
    name: 'pingjia',
    component: () => import('../components/jingtai_/pingjia.vue')
  },
  //订单详情
  {
    path: '/dingdanxiangqing',
    name: 'dingdanxiangqing',
    component: () => import('../components/jingtai_/dingdanxiangqing/dingdanxiangqing.vue')
  },
  //订单详情1
  {
    path: '/xiangqing1/:childlist',
    name: 'xiangqing1',
    component: () => import('../components/jingtai_/dingdanxiangqing/xiangqing1.vue')
  },
  //订单通知
  {
    path: '/dingdantongzhi',
    name: 'dingdantongzhi',
    component: () => import('../components/jingtai_/dingdanxiangqing/dingdantongzhi.vue')
  },
  //挑选车型
  {
    path: '/tiaoxuanchexing',
    name: 'tiaoxuanchexing',
    component: () => import('../components/jingtai_/tiaoxuanchexing.vue'),
    children:[
      // {
      //   path:'/',
      //   component:()=>import('../components/jingtai_/tiaoxuanchexing/tiaoxuan_right.vue')
      // },
      {
        path:':path',
        component:()=>import('../components/jingtai_/tiaoxuanchexing/tiaoxuan_right.vue')
      }
    ]
  },
  //个人信息
  {
    path: '/my_msg',
    name: 'my_msg',
    component: () => import('../components/my_msg/my_msg.vue')
  },
  //密码找回-提交
  {
    path: '/pwdback',
    name: 'pwdback',
    component: () => import('../components/pwdback/pwdback.vue')
  },
  //密码找回-修改
  {
    path: '/surechange',
    name: 'surechange',
    component: () => import('../components/pwdback/surechange.vue')
  },
  //钱包
  {
    path: '/wallet',
    name: 'wallet',
    component: () => import('../components/wallet/Wallet.vue')
  },
  //注册
  {
    path: '/register',
    name: 'register',
    component: () => import('../components/register/register.vue')
  },
  //登录
  {
    path: '/login',
    name: 'login',
    component: () => import('../components/register/login.vue')
  },
  //完善信息
  {
    path: '/userdata',
    name: 'userdata',
    component: () => import('../components/userdata/userdata.vue')
  },  
  //登录后的首页，主要用于广告轮播
  {
    path:'/banner',
    component:() => import('../views/advertising.vue')
  },
 
]

const router = new VueRouter({
  routes,
 
})

export default router
