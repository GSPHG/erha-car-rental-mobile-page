import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    condition:'',
    order: {}, // 订单
    address: {
      provide: "河南省",
      city: "郑州市",
      area: "中原区",
    },
    user:{},
    carinfo:{},
      carId: '',
    startime:'',
    endtime:'',
    startime_:'',
    endtime_:'',
    shopname:'惠济区门店',
    brrowShop:'',
    carid:'',
    list_r:[],
    userbag:{},
    userdata:{}
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
