
import req from '../http.js'; 

// 轮播图
export function carousel(datas) {
    return  req({
        url: '/mk/advertise/queryAll?pageNo=1&pageSize=100',
        method: 'post',
        data: datas,
    })
}

// 轮播图
export function middleImg(datas) {
    return  req({
        url: '/mk/advertise/queryAll?pageNo=1&pageSize=100',
        method: 'post',
        data: datas,
    })
}

// 主页
export function home() {
    return  req({
        url: '/home',
        method: 'get',
    })
}
// 客服问答
export function noob(value) {
    return req({
        url: '/mk/customerIssues/queryAll?pageNo=1&pageSize=100',
        method: 'post',
        data: {
            "submodule": value
        },
        
    })
}
