import axios from 'axios';
// import { config } from 'vue/types/umd';

//请求超时
// axios.defaults.timeout = 10000;

// post请求头
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

function getCookie(objName) {//获取指定名称的cookie的值
    let arrStr = document.cookie.split("; ");
    for (let i = 0; i < arrStr.length; i++) {
        let temp = arrStr[i].split("=");
        if (temp[0] === objName) {
            return decodeURI(temp[1]);
        }
    }
}


export default function (config) {
    // instance:就是实例化的axios对象
    const instance = axios.create({
        //http.js的统一的前缀请求地址
        baseURL: 'http://localhost:12188',
        timeout: 2000
    });

    // 添加请求拦截器
    instance.interceptors.request.use(function (config) {
        console.log(config.url);
        let strings = config.url.split("?");
        // /sso/userLogin/auth
        if (strings[0] !== '/sso/userLogin/auth' &&
            strings[0] !== '/sso/memberLogin/auth' &&
            strings[0] !== '/ms/member/queryMemberByParam'&&
            strings[0] !=='/ms/member/sentSM' &&
            strings[0]!=='/ms/member/insertMember'&&
            strings[0]!=='/ms/member/passwordBack') {
            const token = localStorage.getItem('token')
            if (token) {
                config.headers.myAuthorization = token
            }
        }
        return config;

    }, function (error) {
        // 对请求错误做些什么
        return Promise.reject(error);
    });

    // 添加响应拦截器
    instance.interceptors.response.use(function (response) {
        // 对响应数据做点什么
        return response.data;
    }, function (error) {
        // 对响应错误做点什么
        return Promise.reject(error);
    });

    return instance(config);
}