import req from '../http'
// 侧边栏的全部请求
export function user(data) {
    return  req({
        url: '/api/mbs/member/selectOne',
        method: 'get',
        params:data
    })
}

// 钱包
export function bag(data) {
    return  req({
        url: '/ms/member/selectOne',
        method: 'get',
        params:data
    })
}

//充值
export function addmoney(data){
     return req({
         url:'/ms/account/addMoney',
         method:'post',
         data:data
     })
}
// 支付
export function pay(data){
    console.log(data)
    return req({
        url:'/cs/order/insert',
        method:'post',
        data:data
    })
}
export function queryCarById(data){
    return req({
        url:'/cs/car/query/'+data,
        method:'get',
    })
}
// 修改密码
export function changePwd(data){
    // console.log(data)
    return req({
        url:'/ms/member/changePassword/'+data.memberId+'/'+data.oldPassword+'/'+data.newPassword,
        method:'get'
    })
}
// 找回密码
export function backPwd(data){
    // console.log(data)
    return req({
        url:'/mbs/client/BackPwd',
        method:'post',
        data:data
    })
   
}
// 完善个人资料
export function userdataFun(data){
    // console.log(data)
    return req({
        url:'/ms/member/updateFront',
        method:'put',
        data:data
    })
      
}

//查询个人订单
export function queryOrderByMid(data){
    return req({
        url:'/cs/order/queryOrderByMid/'+data,
        method:'get',
    })
      
}

//查询个人订单详情的车辆配置
export function queryCarConfigByConfigId(data) {
    return req({
        url: '/cs/carConfig/query/' + data,
        method: 'get',
    })
}