import  req from '../http';


//register页面携带手机号获取验证码
export function getCode(datas){
    return req({
        url:"/ms/member/sentSM?phone="+datas.phone,
        method:"get",
    })
}
//判断账号是否存在
export function isExists(datas) {
    console.log(datas)
    return  req({
        url: '/ms/member/queryMemberByParam?phoneNum='+datas.phoneNum,
        method: 'get'
    })
}

// 注册register
export function register(datas) {
    console.log(datas)
    return  req({
        url: '/ms/member/insertMember?phoneNum='+datas.phoneNum+'&password='+datas.password+'&code='+datas.code,
        method: 'get'
    })
}
// 登录
export function login(datas) {
    return req({
        url:'/sso/memberLogin/auth?userName='+datas.userName+'&password='+datas.passWord,
        method: 'get' 
    })
}

//忘记密码pwdback
export function pwdback(datas) {
    return req({
        url:'/ms/member/passwordBack?phoneNum='+datas.phoneNum+'&password='+datas.password+'&code='+datas.code+'',
        method: 'get',
    })
}
