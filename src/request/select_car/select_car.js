import  req from '../http';
// 挑选车型左边
export function select() {
  
    return  req({
        url: '/cs/carType/queryAll?pageNum=1&pageSize=1000',
        method: 'post',
        data:{}
    })
}
// 挑选车型右边
export function right(data) {
    return  req({
        url: '/cs/car/queryCarByRentalConditions',
        method: 'post',
        data:data
    })
}