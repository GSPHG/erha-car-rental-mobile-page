module.exports = {
    publicPath:'./',
    configureWebpack:{
        // 配置路径别名
        resolve: {
            alias: {
                views: '@/view',
                components: '@/components',
                assets:'@/assets'
            }
        }
    }
    // ,
    // devServer: {
    //     ///配置代理服务器，解决跨越问题
    //     proxy: {
    //         '/api': {
    //             target: 'http://localhost:80',
    //             changeOrigin: true,
    //             pathRewrite: {
    //                 '^/api':''
    //             }
    //         },
            
    //     }
    
    // }
}